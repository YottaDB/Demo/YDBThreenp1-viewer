# threenp1-viewer

This code is used to render a visual representation of the calculation of numbers for the Collatz Conjucture.

## Usage:

Step 0: setup yottadb, make sure your shell is configured

Step 1: Run the web server

```
cd threenp1-viewer
go run main.go
```

You should not be able to connect to http://localhost:3000/static/index2.html

Step 2: Run the threenp1 program

```
cd ydbrust-threenp1
echo "100000" | cargo run
```

As this command is running, you should see updates in the web browser
