function drawGraph(element,address) {
  let last_update = "0";
  let interval = 1000;

  const canvas = document.getElementById(element);
  const ctx = canvas.getContext('2d');
  ctx.lineWidth = 1;

  (function poll() {
    setTimeout(function() {
      $.ajax({
          url: "http://" + address + "?start_time=" + last_update,
          type: "GET",
          success: function(data) {
            last_update = data["End_time"];

            if (data.Values.length == 0) {
              interval = 10000;
              return;
            } else {
              interval = 1000;
            }

            for(var i in data["Values"]) {
              ctx.fillRect(data["Values"][i]["x"], data["Values"][i]["y"], 3, 3);
            }
          },
          dataType: "json",
          complete: setTimeout(function() { poll() }, interval),
          timeout: 15000
      })
    }, interval);
  })();
}

$(document).ready(function() {
  drawGraph("ramoth","localhost:3000");
  drawGraph("thorin","localhost:3000");
  drawGraph("nougat","localhost:3000");
  drawGraph("saffron","localhost:3000");
});

