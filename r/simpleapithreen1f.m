;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;								;
; Copyright (c) 2015-2016 Fidelity National Information		;
; Services, Inc. and/or its subsidiaries. All rights reserved.	;
;								;
; Copyright (c) 2017-2018 YottaDB LLC. and/or its subsidiaries.	;
; All rights reserved.						;
;								;
;	This source code contains the intellectual property	;
;	of its copyright holder(s), and is made available	;
;	under a license.  If you do not know the terms of	;
;	the license, please stop and do not read further.	;
;								;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
simpleapithreeen1f
        ; Find the maximum number of steps for the 3n+1 problem for all integers through two input integers.
        ; See http://docs.google.com/View?id=dd5f3337_24gcvprmcw
        ; Assumes input format is 3 integers separated by a space, of which only the first is required:
	;  - the largest number for which the 3n+1 sequence must be computed
	;  - the number of parallel exection streams (defaults to 2× the number of CPUs)
        ;  - the sizes of blocks of integers on which spawned child processes operate
	;    (defaults to approximately the range divided by the number execution streams)
	;    If the block size is larger than the range divided by the number of execution streams,
	;    it is reduced to that value.
        ; No input error checking is done.

        ; This version is adapted and simplified to act as a template from which versions can be created
	; in other languages, in adddition to being a reference implementation for comparison.

        ; Variables do not have to be declared before use, but are New'd in subprograms to ensure that they
        ; do not conflict with names in the caller.

        ; Get number of CPUs from /proc/cpuinfo and calculate default number of execution streams
	set io=$io
	open "/proc/cpuinfo":readonly use "/proc/cpuinfo"
	for  read line quit:$zeof  if 10=$zfind(line,"processor")&$increment(streams)
	use io close "/proc/cpuinfo"
        set streams=2*streams

        ; At the top level, the program reads and processes input lines, one at a time.  Each line specifies
        ; one problem to solve.  Since the program is designed to resume after a crash and reuse partial
        ; results computed before the crash, data in the database at the beginning is assumed to be partial
        ; results from the previous run.  After computing and writing results for a line, the database is
        ; cleared for next line of input or next run of the program.

        ; Loop for ever, read a line (quit on end of file or an empty line), process that line
        for  read input quit:$zeof!'$length(input)  do  ; input has entire input line
        . set j=+$zpiece(input," ",1)                   ; j is ending integer for the problem
        . write $fnumber(j,",",0)     			; print ending integer, formatting with commas
        . set k=+$zpiece(input," ",2)                   ; k second number on input line is optional number streams
	. set:'k k=streams
        . write " ",$fnumber(k,",",0)
        . set blk=+$zpiece(input," ",3)                         ; blk - size of blocks of integers is optional third piece
        . set maxblk=(j-1+k)\k                                  ; default / maximum block size
        . if blk&(blk<=maxblk) write " ",$fnumber(blk,",",0)    ; print block size, optionally corrected
        . else  do
        . . write " (",$fnumber(blk,",",0)
        . . set blk=maxblk
        . . write "->",$FNumber(blk,",",0),")"
        . ; Define blocks of integers for child processes to work on
        . kill ^limits
        . set tmp=0
        . for i=1:1 quit:tmp=j  do
        . . set ^limits(i)=$increment(tmp,blk)
        . . set:tmp>j (tmp,^limits(i))=j
        . ; Launch jobs.  Grab lock l1, atomically increment counter, compute and launch one job for each block of numbers.
        . ; Each child job locks l2(pid), decrements the counter and tries to grab lock l1(pid).
        . ; When counter is zero, all jobs have started.  Parent releases lock l1 and tries to grab lock l2.
        . ; When all children have released their l2(pid) locks, they're done and parent can gather & report results.
        . set ^count=0                                  ; Clear ^count - may have residual value if restarting from crash
        . set def=$ztrnlnm("ydb_tmp") set:'$length(def) def=$ztrnlnm("PWD")     ; Working directory for Jobbed process
        . lock +l1                                      ; Set lock for process synchronization
        . for s=0:1:k-1 do
        . . set c=$increment(^count)                    ; Atomic increment of counter in database for process synchronization
        . . set err=$text(+0)_"_"_$job_"_"_s_".mje"	; stderr for jobbed process
        . . set out=$extract(err,1,$zLength(err)-1)_"o" ; stdout for jobbed process
        . . set cmd="doblk(s):(error="""_err_""":output="""_out_""":default="""_def_""")"     ; Command to job
        . . job @cmd					; Job child process for next block of numbers
	. . set childpid(s)=$zjob			; Note child pids
        . for i=1:1:3000 quit:'^count  hang 0.1		; Wait up to 5 minutes for processes to start (^count goes to 0 when they do)
	. if ^count write !,^count," jobs did not start"
        . lock -l1                              ; Release lock so jobbed processes can run
        . set startat=$zut			; Get starting time
        . lock +l2
        . ; When parent gets lock l2, child processes have completed and parent gathers and reports results.
        . set endat=$zut                     	 ; Get ending time - time between startat and endat is the elapsed time
        . ; Calculate duration
        . Set duration=endat-startat/1E6	; Elapsed time in seconds
        . write " ",$fnumber(^result,",",0)     ; Show largest number of steps for the range i through j
        . write " ",$fnumber(^highest,",",0)    ; Show the highest number reached during the computation
        . write " ",$fnumber(duration,",",3)	; Display elapsed time in seconds with millisecond resolution
        . Write " ",$FNumber(^updates,",",0)    ; Show number of updates
        . Write " ",$FNumber(^reads,",",0)      ; Show number of reads
        . ; If duration is greater than 0, display update and read rates
        . write:duration " ",$fnumber(^updates/duration,",",0)," ",$fnumber(^reads/duration,",",0)
        . write !
        . lock -l2                              ; Release lock for next run
	. ; Wait for child processes to terminate before moving on to next run
        . for s=0:1:k-1 do ^waitforproctodie(childpid(s),300)
        . do dbinit                             ; Initialize database for next run
        quit

dbinit  ; Entryref dbinit clears database between lines of input
        set (^count,^highest,^reads,^result,^updates)=0
	kill ^step
        quit

; This is where Jobbed processes start
doblk(index)
        set (reads,updates,highest)=0           ; Start with zero reads, writes and highest number
        lock +l2($job)                          ; Get lock l2 that parent will wait on till this jobbed processes is done
        if $increment(^count,-1)                ; Decrement ^count to say this process is alive
        lock +l1($job)                          ; This process will get lock l1($job) only when parent has released lock on l1
        ;
        ; Process the next block in ^limits that needs processing; quit when no more blocks to process
	; ^limits(index,1)=0 means that that block defined by ^limits(index) has not been claimed by a job for processing
	; long line following is unavoidable - it is a loop with an exit clause and one statement
        for  quit:'$data(^limits($increment(index)))  do:1=$increment(^limits(index,1)) dostep($select(1<index:^limits(index-1)+1,1:1),^limits(index))
        ;
        tstart ():transactionid="batch"         ; Update global statistics inside a transaction
        ; The following line unconditionally adds the number of reads & write performed by this process to the
        ; number of reads & writes performed by all processes, and sets the highest for all processes if the
        ; highest calculated by this process is greater than that calculated so far for all processes
        set:$increment(^reads,reads)&$increment(^updates,updates)&(highest>$get(^highest)) ^highest=highest
        tcommit
        lock -l1($job),-l2($job)                ; Release locks to tell parent this parent is done
        quit                                    ; Jobbed processes terminate here

dostep(first,last)                              ; Calculate the maximum number of steps from first through last
        new current,currpath,i,n
        for current=first:1:last do
        . set n=current                         ; Start n at current
        . kill currpath                         ; Currpath holds path to 1 for current
        . ; Go till we reach 1 or a number with a known number of steps
        . for i=0:1 quit:$increment(reads)&($data(^step(n))!(1=n))  do
        . . set currpath(i)=n                   ; log n as current number in sequence
        . . set n=$select('(n#2):n/2,1:3*n+1)   ; compute the next number
        . . set:n>highest highest=n             ; see if we have a new highest number reached
        . do:0<i                                ; if 0=i we already have an answer for n, nothing to do here
        . . if 1<n set i=i+^step(n)
        . . tstart ():transactionid="batch"     ; Atomically set maximum
        . . set:i>$get(^result) ^result=i
        . . tcommit
        . . set n="" for  set n=$order(currpath(n)) quit:""=n  set:$increment(updates) ^step(currpath(n))=i-n,^stepXref($J($ZUT/1000,0,0),currpath(n))=i-n
        quit

